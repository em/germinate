#!/bin/bash
# This script needs to be executed from the germinate artifacts directory

set -e

output_dir="output"
development_dir="development"
target_dir="target"
sdk_dir="sdk"
apertis_version="v2024dev2"

generate_dependencies() {
  # Generate list for $1 component
  mkdir "$1"
  cd "$1"

  # Extract dependencies
  awk 'NR>2 { print $1 }' "../${output_dir}/$1.sources" > "$1-run-dependencies.txt"
  awk 'NR>2 { print $1 }' "../${output_dir}/$1.build-sources" > "$1-build-dependencies.txt"

  cd ..
}

generate_lists() {
  mkdir all
  cd all
  cat "../development/development-build-dependencies.txt" "../target/target-build-dependencies.txt" "../sdk/sdk-build-dependencies.txt" > "all-build-dependencies.txt"
  cat "../development/development-run-dependencies.txt" "../target/target-run-dependencies.txt" "../sdk/sdk-run-dependencies.txt" > "all-run-dependencies.txt"
  cat "all-build-dependencies.txt" "all-run-dependencies.txt" > "all-build-run-dependencies.txt"

  # Apertis package list for all components
  osc ls "apertis:${apertis_version}:development" >> "apertis-sources-all.txt"
  osc ls "apertis:${apertis_version}:target" >> "apertis-sources-all.txt"
  osc ls "apertis:${apertis_version}:sdk" >> "apertis-sources-all.txt"

  # Extract the list of common packages, packages to exclude and new packages
  comm -12 <(sort -u "all-build-run-dependencies.txt") <(sort -u "apertis-sources-all.txt") > "existing-packages-to-include.txt"
  comm -13 <(sort -u "all-build-run-dependencies.txt") <(sort -u "apertis-sources-all.txt") > "existing-packages-to-exclude.txt"
  comm -23 <(sort -u "all-build-run-dependencies.txt") <(sort -u "apertis-sources-all.txt") > "new-packages-to-include.txt"
  cd ..
}

generate_component_list() {
  # Generate list for $1 component
  mkdir -p "$1"
  cd "$1"

   # Apertis package list per component
  osc ls "apertis:${apertis_version}:$1" >> "apertis-sources-$1.txt"

  # Merge run and build dependencies into a single file for each component
  cat "$1-run-dependencies.txt" "$1-build-dependencies.txt" > "$1-merged.txt"

  # Extract the list of common packages, packages to exclude and new packages
  comm -12 <(sort -u "apertis-sources-$1.txt") <(sort -u "../all/existing-packages-to-include.txt") > "$1-existing-packages-to-include.txt"
  comm -12 <(sort -u "apertis-sources-$1.txt") <(sort -u "../all/existing-packages-to-exclude.txt") > "$1-existing-packages-to-exclude.txt"
  comm -23 <(sort -u "$1-merged.txt") <(sort -u "../all/apertis-sources-all.txt") > "$1-new-packages-to-include.txt"
  cd ..
}

# Remove old generated files
rm -rf ./all ./development/ ./sdk/ ./target/

# Generate dependencies for development, target, sdk component
generate_dependencies "$development_dir"
generate_dependencies "$target_dir"
generate_dependencies "$sdk_dir"

# Generate list for development, target, sdk component
generate_lists
generate_component_list "$development_dir"
generate_component_list "$target_dir"
generate_component_list "$sdk_dir"
